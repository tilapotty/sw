﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;   

namespace ReadSerialWinForms
{
    /// <summary>
    /// Ez a segédosztály felelős a nyomtatás megvalósításáért.
    /// </summary>
    class Lpt
    {
        /// <summary>
        /// Meghívja az OutPut metódust az inpout32.dll fájlból.
        /// </summary>
        /// <param name="adress">Programunkban ez a 888-as nyomtató portcím lesz.</param>
        /// <param name="value">Ezzel a számmal adhatjuk meg, hogy melyik adatlábra küldjük ki a jelet.
        /// Ezekből a lábakból 8 van és a 2 hatványaival tudjuk kiküldeni a jelet.</param>
        [DllImport("inpout32.dll", EntryPoint = "Out32")]
        public static extern void Output(int adress, int value);

        /// <summary>
        /// Meghívja az Input metódust az inpout32.dll fájlból.
        /// </summary>
        [DllImport("inpout32.dll", EntryPoint = "Inp32")]
        public static extern void Input(int adress);
    }
}
