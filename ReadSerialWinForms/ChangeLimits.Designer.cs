﻿namespace ReadSerialWinForms
{
    partial class ChangeLimits
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbPW = new System.Windows.Forms.TextBox();
            this.btnPwOk = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbSettings = new System.Windows.Forms.GroupBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnIGDdel = new System.Windows.Forms.Button();
            this.actIGD = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btnAddIGD = new System.Windows.Forms.Button();
            this.txbNewItemgroupdesc = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.tbIP = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbxShutterUs = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnSettSave = new System.Windows.Forms.Button();
            this.tbLimit7 = new System.Windows.Forms.TextBox();
            this.tbLimit6 = new System.Windows.Forms.TextBox();
            this.tbLimit5 = new System.Windows.Forms.TextBox();
            this.tbLimit4 = new System.Windows.Forms.TextBox();
            this.tbLimit3 = new System.Windows.Forms.TextBox();
            this.tbLimit2 = new System.Windows.Forms.TextBox();
            this.tbLimit1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbItemGroupDesc = new System.Windows.Forms.ComboBox();
            this.tbPPPLGR = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txbKameraIP = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.gbSettings.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jelszó:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(6, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Hibás jelszó!";
            this.label2.Visible = false;
            // 
            // txtbPW
            // 
            this.txtbPW.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtbPW.Location = new System.Drawing.Point(10, 49);
            this.txtbPW.Name = "txtbPW";
            this.txtbPW.PasswordChar = '*';
            this.txtbPW.Size = new System.Drawing.Size(100, 21);
            this.txtbPW.TabIndex = 2;
            this.txtbPW.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbPW_KeyPress);
            // 
            // btnPwOk
            // 
            this.btnPwOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPwOk.Location = new System.Drawing.Point(119, 47);
            this.btnPwOk.Name = "btnPwOk";
            this.btnPwOk.Size = new System.Drawing.Size(75, 23);
            this.btnPwOk.TabIndex = 3;
            this.btnPwOk.Text = "OK";
            this.btnPwOk.UseVisualStyleBackColor = true;
            this.btnPwOk.Click += new System.EventHandler(this.btnPwOk_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnPwOk);
            this.groupBox1.Controls.Add(this.txtbPW);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(288, 134);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "A módosításokhoz jelszó szükséges!";
            // 
            // gbSettings
            // 
            this.gbSettings.Controls.Add(this.txbKameraIP);
            this.gbSettings.Controls.Add(this.label18);
            this.gbSettings.Controls.Add(this.lblMessage);
            this.gbSettings.Controls.Add(this.groupBox2);
            this.gbSettings.Controls.Add(this.btnClose);
            this.gbSettings.Controls.Add(this.tbIP);
            this.gbSettings.Controls.Add(this.label15);
            this.gbSettings.Controls.Add(this.tbxShutterUs);
            this.gbSettings.Controls.Add(this.label14);
            this.gbSettings.Controls.Add(this.label13);
            this.gbSettings.Controls.Add(this.label12);
            this.gbSettings.Controls.Add(this.btnSettSave);
            this.gbSettings.Controls.Add(this.tbLimit7);
            this.gbSettings.Controls.Add(this.tbLimit6);
            this.gbSettings.Controls.Add(this.tbLimit5);
            this.gbSettings.Controls.Add(this.tbLimit4);
            this.gbSettings.Controls.Add(this.tbLimit3);
            this.gbSettings.Controls.Add(this.tbLimit2);
            this.gbSettings.Controls.Add(this.tbLimit1);
            this.gbSettings.Controls.Add(this.label11);
            this.gbSettings.Controls.Add(this.label10);
            this.gbSettings.Controls.Add(this.label9);
            this.gbSettings.Controls.Add(this.label8);
            this.gbSettings.Controls.Add(this.label7);
            this.gbSettings.Controls.Add(this.label6);
            this.gbSettings.Controls.Add(this.label5);
            this.gbSettings.Controls.Add(this.label4);
            this.gbSettings.Controls.Add(this.cbItemGroupDesc);
            this.gbSettings.Controls.Add(this.tbPPPLGR);
            this.gbSettings.Controls.Add(this.label3);
            this.gbSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.gbSettings.Location = new System.Drawing.Point(13, 153);
            this.gbSettings.Name = "gbSettings";
            this.gbSettings.Size = new System.Drawing.Size(617, 497);
            this.gbSettings.TabIndex = 5;
            this.gbSettings.TabStop = false;
            this.gbSettings.Text = "Beállítások";
            this.gbSettings.Visible = false;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.ForeColor = System.Drawing.Color.Red;
            this.lblMessage.Location = new System.Drawing.Point(316, 218);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(58, 20);
            this.lblMessage.TabIndex = 27;
            this.lblMessage.Text = "uzenet";
            this.lblMessage.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnIGDdel);
            this.groupBox2.Controls.Add(this.actIGD);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.btnAddIGD);
            this.groupBox2.Controls.Add(this.txbNewItemgroupdesc);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Location = new System.Drawing.Point(306, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(305, 186);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Fafajok kezelése";
            // 
            // btnIGDdel
            // 
            this.btnIGDdel.Location = new System.Drawing.Point(10, 148);
            this.btnIGDdel.Name = "btnIGDdel";
            this.btnIGDdel.Size = new System.Drawing.Size(289, 26);
            this.btnIGDdel.TabIndex = 5;
            this.btnIGDdel.Text = "Aktuális fafaj törlése";
            this.btnIGDdel.UseVisualStyleBackColor = true;
            this.btnIGDdel.Click += new System.EventHandler(this.btnIGDdel_Click);
            // 
            // actIGD
            // 
            this.actIGD.AutoSize = true;
            this.actIGD.Location = new System.Drawing.Point(179, 119);
            this.actIGD.Name = "actIGD";
            this.actIGD.Size = new System.Drawing.Size(61, 20);
            this.actIGD.TabIndex = 4;
            this.actIGD.Text = "actIGD";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 119);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(104, 20);
            this.label17.TabIndex = 3;
            this.label17.Text = "Aktuális fafaj:";
            // 
            // btnAddIGD
            // 
            this.btnAddIGD.Location = new System.Drawing.Point(10, 84);
            this.btnAddIGD.Name = "btnAddIGD";
            this.btnAddIGD.Size = new System.Drawing.Size(289, 26);
            this.btnAddIGD.TabIndex = 2;
            this.btnAddIGD.Text = "Hozzáadás";
            this.btnAddIGD.UseVisualStyleBackColor = true;
            this.btnAddIGD.Click += new System.EventHandler(this.btnAddIGD_Click);
            // 
            // txbNewItemgroupdesc
            // 
            this.txbNewItemgroupdesc.Location = new System.Drawing.Point(114, 40);
            this.txbNewItemgroupdesc.Name = "txbNewItemgroupdesc";
            this.txbNewItemgroupdesc.Size = new System.Drawing.Size(185, 26);
            this.txbNewItemgroupdesc.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 41);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(101, 20);
            this.label16.TabIndex = 0;
            this.label16.Text = "Új fafaj neve:";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(306, 454);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(220, 30);
            this.btnClose.TabIndex = 25;
            this.btnClose.Text = "Bezárás";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tbIP
            // 
            this.tbIP.Location = new System.Drawing.Point(128, 333);
            this.tbIP.Name = "tbIP";
            this.tbIP.Size = new System.Drawing.Size(153, 26);
            this.tbIP.TabIndex = 24;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 336);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 20);
            this.label15.TabIndex = 23;
            this.label15.Text = "IP cím:";
            // 
            // tbxShutterUs
            // 
            this.tbxShutterUs.Location = new System.Drawing.Point(128, 365);
            this.tbxShutterUs.Name = "tbxShutterUs";
            this.tbxShutterUs.Size = new System.Drawing.Size(153, 26);
            this.tbxShutterUs.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 368);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 20);
            this.label14.TabIndex = 21;
            this.label14.Text = "ShutterUs:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(485, 368);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 20);
            this.label13.TabIndex = 20;
            this.label13.Text = "label13";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(302, 368);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(168, 20);
            this.label12.TabIndex = 19;
            this.label12.Text = "Legutóbbi gyalult fafaj:";
            // 
            // btnSettSave
            // 
            this.btnSettSave.Location = new System.Drawing.Point(10, 454);
            this.btnSettSave.Name = "btnSettSave";
            this.btnSettSave.Size = new System.Drawing.Size(267, 30);
            this.btnSettSave.TabIndex = 18;
            this.btnSettSave.Text = "Mentés";
            this.btnSettSave.UseVisualStyleBackColor = true;
            this.btnSettSave.Click += new System.EventHandler(this.btnSettSave_Click);
            // 
            // tbLimit7
            // 
            this.tbLimit7.Location = new System.Drawing.Point(128, 301);
            this.tbLimit7.Name = "tbLimit7";
            this.tbLimit7.Size = new System.Drawing.Size(153, 26);
            this.tbLimit7.TabIndex = 17;
            // 
            // tbLimit6
            // 
            this.tbLimit6.Location = new System.Drawing.Point(128, 269);
            this.tbLimit6.Name = "tbLimit6";
            this.tbLimit6.Size = new System.Drawing.Size(153, 26);
            this.tbLimit6.TabIndex = 16;
            // 
            // tbLimit5
            // 
            this.tbLimit5.Location = new System.Drawing.Point(128, 237);
            this.tbLimit5.Name = "tbLimit5";
            this.tbLimit5.Size = new System.Drawing.Size(153, 26);
            this.tbLimit5.TabIndex = 15;
            // 
            // tbLimit4
            // 
            this.tbLimit4.Location = new System.Drawing.Point(128, 205);
            this.tbLimit4.Name = "tbLimit4";
            this.tbLimit4.Size = new System.Drawing.Size(153, 26);
            this.tbLimit4.TabIndex = 14;
            // 
            // tbLimit3
            // 
            this.tbLimit3.Location = new System.Drawing.Point(128, 173);
            this.tbLimit3.Name = "tbLimit3";
            this.tbLimit3.Size = new System.Drawing.Size(153, 26);
            this.tbLimit3.TabIndex = 13;
            // 
            // tbLimit2
            // 
            this.tbLimit2.Location = new System.Drawing.Point(128, 141);
            this.tbLimit2.Name = "tbLimit2";
            this.tbLimit2.Size = new System.Drawing.Size(153, 26);
            this.tbLimit2.TabIndex = 12;
            // 
            // tbLimit1
            // 
            this.tbLimit1.Location = new System.Drawing.Point(128, 109);
            this.tbLimit1.Name = "tbLimit1";
            this.tbLimit1.Size = new System.Drawing.Size(153, 26);
            this.tbLimit1.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 304);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 20);
            this.label11.TabIndex = 10;
            this.label11.Text = "Határ 7.:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 272);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 20);
            this.label10.TabIndex = 9;
            this.label10.Text = "Határ 6.:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 240);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "Határ 5.:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 208);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "Határ 4.:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 176);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Határ 3.:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 144);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Határ 2.:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Határ 1.:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Fafaj:";
            // 
            // cbItemGroupDesc
            // 
            this.cbItemGroupDesc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbItemGroupDesc.FormattingEnabled = true;
            this.cbItemGroupDesc.Location = new System.Drawing.Point(128, 63);
            this.cbItemGroupDesc.Name = "cbItemGroupDesc";
            this.cbItemGroupDesc.Size = new System.Drawing.Size(153, 28);
            this.cbItemGroupDesc.TabIndex = 2;
            this.cbItemGroupDesc.SelectedIndexChanged += new System.EventHandler(this.cbItemGroupDesc_SelectedIndexChanged);
            // 
            // tbPPPLGR
            // 
            this.tbPPPLGR.Location = new System.Drawing.Point(128, 23);
            this.tbPPPLGR.Name = "tbPPPLGR";
            this.tbPPPLGR.Size = new System.Drawing.Size(153, 26);
            this.tbPPPLGR.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Gépazonosító:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(9, 404);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 20);
            this.label18.TabIndex = 28;
            this.label18.Text = "Kamera IP:";
            // 
            // txbKameraIP
            // 
            this.txbKameraIP.Location = new System.Drawing.Point(128, 401);
            this.txbKameraIP.Name = "txbKameraIP";
            this.txbKameraIP.Size = new System.Drawing.Size(153, 26);
            this.txbKameraIP.TabIndex = 29;
            // 
            // ChangeLimits
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 662);
            this.Controls.Add(this.gbSettings);
            this.Controls.Add(this.groupBox1);
            this.Name = "ChangeLimits";
            this.Text = "Módosítások";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbSettings.ResumeLayout(false);
            this.gbSettings.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbPW;
        private System.Windows.Forms.Button btnPwOk;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbSettings;
        private System.Windows.Forms.TextBox tbPPPLGR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbItemGroupDesc;
        private System.Windows.Forms.TextBox tbLimit7;
        private System.Windows.Forms.TextBox tbLimit6;
        private System.Windows.Forms.TextBox tbLimit5;
        private System.Windows.Forms.TextBox tbLimit4;
        private System.Windows.Forms.TextBox tbLimit3;
        private System.Windows.Forms.TextBox tbLimit2;
        private System.Windows.Forms.TextBox tbLimit1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSettSave;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbxShutterUs;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbIP;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txbNewItemgroupdesc;
        private System.Windows.Forms.Button btnAddIGD;
        private System.Windows.Forms.Button btnIGDdel;
        private System.Windows.Forms.Label actIGD;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.TextBox txbKameraIP;
        private System.Windows.Forms.Label label18;
    }
}