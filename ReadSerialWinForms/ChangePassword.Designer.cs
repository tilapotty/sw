﻿namespace ReadSerialWinForms
{
    partial class ChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPwOk = new System.Windows.Forms.Button();
            this.txtbPW = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnNewPwSave = new System.Windows.Forms.Button();
            this.txbNewPw2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txbNewPw1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnPwOk);
            this.groupBox1.Controls.Add(this.txtbPW);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(299, 106);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "A módosításokhoz jelszó szükséges!";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jelszó:";
            // 
            // btnPwOk
            // 
            this.btnPwOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPwOk.Location = new System.Drawing.Point(213, 49);
            this.btnPwOk.Name = "btnPwOk";
            this.btnPwOk.Size = new System.Drawing.Size(75, 23);
            this.btnPwOk.TabIndex = 3;
            this.btnPwOk.Text = "OK";
            this.btnPwOk.UseVisualStyleBackColor = true;
            this.btnPwOk.Click += new System.EventHandler(this.btnPwOk_Click);
            // 
            // txtbPW
            // 
            this.txtbPW.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtbPW.Location = new System.Drawing.Point(10, 49);
            this.txtbPW.Name = "txtbPW";
            this.txtbPW.PasswordChar = '*';
            this.txtbPW.Size = new System.Drawing.Size(197, 21);
            this.txtbPW.TabIndex = 2;
            this.txtbPW.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbPW_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(6, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Hibás jelszó!";
            this.label2.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.btnNewPwSave);
            this.groupBox2.Controls.Add(this.txbNewPw2);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txbNewPw1);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(12, 125);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(299, 166);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Új jelszó megadása";
            this.groupBox2.Visible = false;
            // 
            // btnNewPwSave
            // 
            this.btnNewPwSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnNewPwSave.Location = new System.Drawing.Point(213, 105);
            this.btnNewPwSave.Name = "btnNewPwSave";
            this.btnNewPwSave.Size = new System.Drawing.Size(75, 23);
            this.btnNewPwSave.TabIndex = 4;
            this.btnNewPwSave.Text = "Mentés";
            this.btnNewPwSave.UseVisualStyleBackColor = true;
            this.btnNewPwSave.Click += new System.EventHandler(this.btnNewPwSave_Click);
            // 
            // txbNewPw2
            // 
            this.txbNewPw2.Location = new System.Drawing.Point(10, 102);
            this.txbNewPw2.Name = "txbNewPw2";
            this.txbNewPw2.PasswordChar = '*';
            this.txbNewPw2.Size = new System.Drawing.Size(197, 26);
            this.txbNewPw2.TabIndex = 3;
            this.txbNewPw2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txbNewPw2_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(172, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "Új jelszó megerősítése:";
            // 
            // txbNewPw1
            // 
            this.txbNewPw1.Location = new System.Drawing.Point(10, 49);
            this.txbNewPw1.Name = "txbNewPw1";
            this.txbNewPw1.PasswordChar = '*';
            this.txbNewPw1.Size = new System.Drawing.Size(197, 26);
            this.txbNewPw1.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Új jelszó:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(10, 135);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(283, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "Nem egyezik meg a megadott új jelszó!";
            this.label5.Visible = false;
            // 
            // ChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 303);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ChangePassword";
            this.Text = "Jelszó módosítás";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPwOk;
        private System.Windows.Forms.TextBox txtbPW;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnNewPwSave;
        private System.Windows.Forms.TextBox txbNewPw2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txbNewPw1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
    }
}