﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace ReadSerialWinForms
{
    public partial class ChangePassword : Form
    {
        Hashtable tempHT;

        public ChangePassword()
        {
            InitializeComponent();
            tempHT = new Hashtable();
        }

        private void btnPwOk_Click(object sender, EventArgs e)
        {
            ValidPassword();
        }
        
        private void txtbPW_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
                ValidPassword();
        }

        private void ValidPassword()
        {
            bool isOK;
            using (MD5 md5Hash = MD5.Create())
            {
                isOK = PortReader.VerifyMd5Hash(md5Hash, txtbPW.Text);
                if (isOK)
                {
                    groupBox2.Visible = true;
                    groupBox1.Enabled = false;
                    label2.Visible = false;
                }
                else
                {
                    label2.Visible = true;
                }
            }
        }

        private void btnNewPwSave_Click(object sender, EventArgs e)
        {
            NewPassword();
        }

        private void NewPassword()
        {
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (comparer.Compare(txbNewPw1.Text, txbNewPw2.Text) == 0)
            {
                label5.Visible = false;
                using (MD5 md5Hash = MD5.Create())
                {
                    string hash = PortReader.GetMd5Hash(md5Hash, txbNewPw1.Text);
                    tempHT.Add("Passwd", hash);

                    FileStream fs = new FileStream(@"C:\Egyetem\Password.dat", FileMode.Create);

                    BinaryFormatter formatter = new BinaryFormatter();
                    try
                    {
                        formatter.Serialize(fs, tempHT);
                    }
                    catch (SerializationException e)
                    {
                        MessageBox.Show("Failed to serialize. Reason: " + e.Message);
                        throw;
                    }
                    finally
                    {
                        fs.Close();
                    }
                }
            }
            else
            {
                label5.Visible = true;
            }
            this.Close();
        }

        private void txbNewPw2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
                NewPassword();
        }
    }
}
