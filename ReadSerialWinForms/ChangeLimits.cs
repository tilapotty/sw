﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Diagnostics;
using System.Net;

namespace ReadSerialWinForms
{
    public partial class ChangeLimits : Form
    {
        public static Hashtable hashDataTable = null;
        public static List<string> spieces = new List<string>();
        private PortReader myPortReader = new PortReader();
        public List<string> tempSpieces = new List<string>();

        public ChangeLimits()
        {
            InitializeComponent();
        }
        
        private void btnPwOk_Click(object sender, EventArgs e)
        {
            ValidPassword();
        }
        
        private void ValidPassword()
        {
            bool isOK;
            using (MD5 md5Hash = MD5.Create())
            {
                isOK = PortReader.VerifyMd5Hash(md5Hash, txtbPW.Text);
                if (isOK)
                {
                    label2.Visible = false;
                    gbSettings.Visible = true;
                    groupBox1.Enabled = false;
                    this.FillData();
                }
                else
                {
                    label2.Visible = true;
                }
            }
        }

        [STAThread]
        private void FillData()
        {   
            // Some information
            FileStream fs = new FileStream(@"C:\Egyetem\DataFile.dat", FileMode.Open);

            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                hashDataTable = (Hashtable)formatter.Deserialize(fs);   
            }
            catch
            {
                throw;
            }
            finally
            {
                fs.Close();
            }
            
            // Spieces and limits
            FileStream fs2 = new FileStream(@"C:\Egyetem\DataFile2.dat", FileMode.Open);
            BinaryFormatter formatter2 = new BinaryFormatter();
            try
            {
                spieces = (List<string>)formatter2.Deserialize(fs2);
                // If changed everything, then load back to the file
                tempSpieces.AddRange(spieces);
            }
            catch
            {
                throw;
            }
            finally
            {
                fs2.Close();
            }

            UpdateSpieces();

            tbPPPLGR.Text = hashDataTable["PPPLGR"].ToString();
            txbKameraIP.Text = (hashDataTable["KameraIP"] == null) ? "" : hashDataTable["KameraIP"].ToString();
            label13.Text = hashDataTable["lastItemGroupDesc"].ToString();
            cbItemGroupDesc.SelectedIndex = 0;
            tbIP.Text = hashDataTable["IPaddress"].ToString();
            //tbxShutterUs.Text = (hashDataTable["KameraIP"] == null) ? "" : hashDataTable["KameraIP"].ToString();
        }

        private void UpdateSpieces()
        {
            cbItemGroupDesc.Items.Clear();
            cbItemGroupDesc.Items.AddRange(spieces.ToArray());
        }

        private void txtbPW_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
                ValidPassword();
        }

        private void cbItemGroupDesc_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateLimits();

            actIGD.Text = cbItemGroupDesc.SelectedItem.ToString();
        }

        private void UpdateLimits()
        {
            string tempItemGroupDesc = cbItemGroupDesc.SelectedItem.ToString();
            string tempHashKey = tempItemGroupDesc + "1G";
            tbLimit1.Text = (hashDataTable["" + tempHashKey + ""] == null) ? "" : hashDataTable["" + tempHashKey + ""].ToString();
            tempHashKey = tempItemGroupDesc + "2G";
            tbLimit2.Text = (hashDataTable["" + tempHashKey + ""] == null) ? "" : hashDataTable["" + tempHashKey + ""].ToString();
            tempHashKey = tempItemGroupDesc + "3G";
            tbLimit3.Text = (hashDataTable["" + tempHashKey + ""] == null) ? "" : hashDataTable["" + tempHashKey + ""].ToString();
            tempHashKey = tempItemGroupDesc + "4G";
            tbLimit4.Text = (hashDataTable["" + tempHashKey + ""] == null) ? "" : hashDataTable["" + tempHashKey + ""].ToString();
            tempHashKey = tempItemGroupDesc + "5G";
            tbLimit5.Text = (hashDataTable["" + tempHashKey + ""] == null) ? "" : hashDataTable["" + tempHashKey + ""].ToString();
            tempHashKey = tempItemGroupDesc + "6G";
            tbLimit6.Text = (hashDataTable["" + tempHashKey + ""] == null) ? "" : hashDataTable["" + tempHashKey + ""].ToString();
            tempHashKey = tempItemGroupDesc + "7G";
            tbLimit7.Text = (hashDataTable["" + tempHashKey + ""] == null) ? "" : hashDataTable["" + tempHashKey + ""].ToString();

            tempHashKey = tempItemGroupDesc + "ShutterUs";
            tbxShutterUs.Text = (hashDataTable["" + tempHashKey + ""] == null) ? "" : hashDataTable["" + tempHashKey + ""].ToString();
        }

        private void btnSettSave_Click(object sender, EventArgs e)
        {
            Serialize();
            //WriteTintAppFile();
            ////DoCopyOnFTP("TintApp.ini");
            //myPortReader.DoCopyOnFTP("TintApp.ini");
        }

        [STAThread]
        private void Serialize()
        {
            // If happened add/delete species --> write into DataFile2.dat file
            if (!spieces.SequenceEqual(tempSpieces))
            {
                FileStream fs2 = new FileStream(@"C:\Egyetem\DataFile2.dat", FileMode.Create);
                BinaryFormatter formatter2 = new BinaryFormatter();
                try
                {
                    formatter2.Serialize(fs2, spieces);
                }
                catch (SerializationException exp)
                {
                    MessageBox.Show("Failed to serialize. Reason: " + exp.Message);
                    throw;
                }
                finally
                {
                    fs2.Close();
                }
            }
            
            hashDataTable["PPPLGR"] = tbPPPLGR.Text;
            //hashDataTable["ShutterUs"] = tbxShutterUs.Text;
            hashDataTable["IPaddress"] = tbIP.Text;
            hashDataTable["KameraIP"] = txbKameraIP.Text;

            if (cbItemGroupDesc.SelectedItem != null)
            {
                string tempItemGroupDesc = cbItemGroupDesc.SelectedItem.ToString();
                
                string tempHashKey = tempItemGroupDesc + "1G";
                hashDataTable["" + tempHashKey + ""] = tbLimit1.Text;
                tempHashKey = tempItemGroupDesc + "2G";
                hashDataTable["" + tempHashKey + ""] = tbLimit2.Text;
                tempHashKey = tempItemGroupDesc + "3G";
                hashDataTable["" + tempHashKey + ""] = tbLimit3.Text;
                tempHashKey = tempItemGroupDesc + "4G";
                hashDataTable["" + tempHashKey + ""] = tbLimit4.Text;
                tempHashKey = tempItemGroupDesc + "5G";
                hashDataTable["" + tempHashKey + ""] = tbLimit5.Text;
                tempHashKey = tempItemGroupDesc + "6G";
                hashDataTable["" + tempHashKey + ""] = tbLimit6.Text;
                tempHashKey = tempItemGroupDesc + "7G";
                hashDataTable["" + tempHashKey + ""] = tbLimit7.Text;

                tempHashKey = tempItemGroupDesc + "ShutterUs";
                hashDataTable["" + tempHashKey + ""] = tbxShutterUs.Text;
            }

            FileStream fs = new FileStream(@"C:\Egyetem\DataFile.dat", FileMode.Create);

            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, hashDataTable);
            }
            catch
            {
                throw;
            }
            finally
            {
                fs.Close();
            }
        }

        private void WriteTintAppFile()
        {
            // File open
            IniFile ini = new IniFile(@"C:\Egyetem\TintApp.ini");

            // Load itemGroupDesc
            string itemGroup = cbItemGroupDesc.SelectedItem.ToString();
            string patternString = "WPMat_";

            // Append
            string finalPattern = patternString + itemGroup.ToUpper();

            // Modify Limits and Shutterus
            ini.Write("ShutterUs", tbxShutterUs.Text, finalPattern);
            ini.Write("ThreshDark", tbLimit1.Text, finalPattern);
            ini.Write("ThreshLight", tbLimit2.Text, finalPattern);

            // Report
            MessageBox.Show("Limit(ek) módosítva!");

            // An example...
            //int rowID;
            //rowID = int.Parse(ini.IniReadValue(finalPattern, "ThreshDark"));
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddIGD_Click(object sender, EventArgs e)
        {
            AddIGD();
        }

        private void AddIGD()
        {
            string newItemgroupdesc = txbNewItemgroupdesc.Text;
            spieces.Add(newItemgroupdesc);
            UpdateSpieces();

            string tempHashKey = "";
            for (int i = 1; i <= 7; i++)
            {
                tempHashKey = newItemgroupdesc + i + "G";
                hashDataTable["" + tempHashKey + ""] = "255";
            }

            cbItemGroupDesc.SelectedItem = newItemgroupdesc;

            lblMessage.Text = "Ne felejtsen el Mentés-t nyomni!";
            lblMessage.Visible = true;
        }

        private void btnIGDdel_Click(object sender, EventArgs e)
        {
            DelIGD();
        }

        [STAThread]
        private void DelIGD()
        {
            string actItemgroupdesc = actIGD.Text;
            spieces.Remove(actItemgroupdesc);
            UpdateSpieces();

            string tempHashKey = "";
            for (int i = 1; i <= 7; i++)
            {
                tempHashKey = actItemgroupdesc + i + "G";
                hashDataTable.Remove(tempHashKey);
            }

            FileStream fs = new FileStream(@"C:\Egyetem\DataFile.dat", FileMode.Create);

            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, hashDataTable);
            }
            catch
            {
                throw;
            }
            finally
            {
                fs.Close();
            }

            // Delete the section of actual itemGroupDesc from TintApp.ini file
            IniFile outputParser = new IniFile(@"C:\Egyetem\TintApp.ini");
            outputParser.DeleteSection("WPMAT_"+actItemgroupdesc);

            if(spieces.Count != 0)
                cbItemGroupDesc.SelectedIndex = 0;
            else
                actIGD.Text = "Nincs törölhető fafaj.";

            lblMessage.Text = "Ne felejtsen el Mentés-t nyomni!";
            lblMessage.Visible = true;
        }
    }
}
