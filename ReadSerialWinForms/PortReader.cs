﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.IO.Ports;
using System.Data;
using FirebirdSql.Data.FirebirdClient;
using System.Collections;
using System.Diagnostics;

namespace ReadSerialWinForms
{
    // Delegates
    /// <summary>
    /// Status messages
    /// </summary>
    public delegate void StatusDelegate();
    /// <summary>
    /// Input messages
    /// </summary>
    public delegate void InputDelegate();

    public enum Where
    {
        SW,
        Home,
        Work
    }

    /// <summary>
    /// It receives data through serial ports.
    /// It contains the most important fields, events and constructors
    /// </summary>
    partial class PortReader
    {
        #region Fields

        //public string where = "Home"; // def & init in Form1.cs file

        // Events
        public event StatusDelegate StatusChangedEvent;
        public event InputDelegate InputChangedEvent;

        // Data through serial ports
        public SerialPort port, port2; // port: camera data, port2: lightbarrier
        public SerialPort com4Port;
        private int value;
        private string input = "";
        public string barrierInput = "";

        // Data from ini files
        private string machineID;
        private int limit1, limit2, limit3, limit4, limit5, limit6, limit7;
        public Hashtable fileEntries = null;
        static Hashtable addresses = null;
        public int timeDelay;

        // Metadata and calculated values
        private int shiftID;
        private string shiftString;
        private DateTime recDate = new DateTime();
        private int year, month, day, hour, minute, second;
        private TimeSpan recTime;
        private string result = "";
        private static string itemGroupDesc = "";
        private int amount = 0;
        private Dictionary<int, int> valueAndAmount = new Dictionary<int, int>();

        // Temporarily values
        private string status = "";
        private int flag = 0;
        
        // Variables for database connection
        private string IPaddress = "";
        private FbConnection conn;
        private string repnoID = "";
        private DataSet myDataSet = new DataSet();
        private DataTable myDataTable = new DataTable("ELOGYALULASLOG"); // For saving into database
        private DataTable myDataTableFile = new DataTable("ELOGYALULASLOGFILE"); // For saving into file
        public readonly bool dbConnIsOpen = true;
        public readonly bool port2IsOpen = true;
        
        // Variables for printing
        int printerValue = 0;
        bool barrierFlag = false;

        #endregion

        #region Properties
        public int Value
        {
            get { return value; } //set { this.value = value; }
        }

        public string IP_Address
        {
            get { return IPaddress; }
            set { IPaddress = value; }
        }

        public FbConnection Conn
        {
            get { return conn; }
            set { conn = value; }
        }
        
        public string Status
        {
            get { return status; } set { status = value; }
        }
        
        public string Input
        {
            get { return input; }
        }
        
        public string Result
        {
            get { return result; }
        }
        
        public string ItemGroupDesc
        {
            get { return itemGroupDesc; }
            set { itemGroupDesc = value; }
        }

        public string RepnoID
        {
            get { return repnoID; }
            set { repnoID = value; }
        }
        #endregion

        /// <summary>
        /// It initializes the serial ports and the dataset/datatable
        /// A konstruktor inicializálja a soros portot, valamint a DataTable-t és a DataSet-et.
        /// </summary>
        public PortReader()
        {
            port = new SerialPort("COM1", 115200, Parity.None, 8, StopBits.One);

            port2 = new SerialPort("COM3", 9600, Parity.None, 8, StopBits.One); // Signal from barrier because of coming of timber board

            com4Port = new SerialPort("COM4", 9600, Parity.None, 8, StopBits.One); // Barrier on/off
            
            port.DataReceived += new SerialDataReceivedEventHandler(Port_DataReceived);

            port2.DataReceived += new SerialDataReceivedEventHandler(Barrier_DataReceived);

            try
            {
                port.Open();
                port2.Open();
                com4Port.Open();
                com4Port.RtsEnable = true;
            }
            catch
            {
                dbConnIsOpen = false;
                port2IsOpen = false;
                throw;
            }
            
            DataColumn cShiftID = new DataColumn("ShiftID", typeof(int));
            DataColumn cShiftString = new DataColumn("ShiftString", typeof(string));
            DataColumn cPPPLGR = new DataColumn("PPPLGR", typeof(string));
            DataColumn cRec_Date = new DataColumn("Rec_Date", typeof(DateTime));
            DataColumn cRec_Time = new DataColumn("Rec_Time", typeof(TimeSpan));
            DataColumn cYear = new DataColumn("Year", typeof(int));
            DataColumn cMonth = new DataColumn("Month", typeof(int));
            DataColumn cDay = new DataColumn("Day", typeof(int));
            DataColumn cHour = new DataColumn("Hour", typeof(int));
            DataColumn cMinute = new DataColumn("Minute", typeof(int));
            DataColumn cSecond = new DataColumn("Second", typeof(int));
            DataColumn cValue = new DataColumn("Value", typeof(int));
            DataColumn cLimit1 = new DataColumn("Limit1", typeof(int));
            DataColumn cLimit2 = new DataColumn("Limit2", typeof(int));
            DataColumn cResult = new DataColumn("Result", typeof(string));
            DataColumn cItemGroup_Desc = new DataColumn("ItemGroup_Desc", typeof(string));
            DataColumn cRepno = new DataColumn("Repno", typeof(string));
            DataColumn cAmount = new DataColumn("Amount", typeof(int));
            DataColumn cLimit3 = new DataColumn("Limit3", typeof(int));
            DataColumn cLimit4 = new DataColumn("Limit4", typeof(int));
            DataColumn cLimit5 = new DataColumn("Limit5", typeof(int));
            DataColumn cLimit6 = new DataColumn("Limit6", typeof(int));
            DataColumn cLimit7 = new DataColumn("Limit7", typeof(int));

            myDataTable.Columns.AddRange(new DataColumn[]
                    { cShiftID, cShiftString, cPPPLGR, cRec_Date, cRec_Time, cYear, cMonth, cDay, cHour, 
                        cMinute, cSecond, cValue, cLimit1, cLimit2, cResult, cItemGroup_Desc, cRepno, cAmount, 
                        cLimit3, cLimit4, cLimit5, cLimit6, cLimit7 });
            
            #region Data write into file - Datatable and columns - for live data saving
            DataColumn cShiftIDFile = new DataColumn("ShiftID", typeof(int));
            DataColumn cShiftStringFile = new DataColumn("ShiftString", typeof(string));
            DataColumn cPPPLGRFile = new DataColumn("PPPLGR", typeof(string));
            DataColumn cRec_DateFile = new DataColumn("Rec_Date", typeof(DateTime));
            DataColumn cRec_TimeFile = new DataColumn("Rec_Time", typeof(TimeSpan));
            DataColumn cYearFile = new DataColumn("Year", typeof(int));
            DataColumn cMonthFile = new DataColumn("Month", typeof(int));
            DataColumn cDayFile = new DataColumn("Day", typeof(int));
            DataColumn cHourFile = new DataColumn("Hour", typeof(int));
            DataColumn cMinuteFile = new DataColumn("Minute", typeof(int));
            DataColumn cSecondFile = new DataColumn("Second", typeof(int));
            DataColumn cValueFile = new DataColumn("Value", typeof(int));
            DataColumn cLimit1File = new DataColumn("Limit1", typeof(int));
            DataColumn cLimit2File = new DataColumn("Limit2", typeof(int));
            DataColumn cResultFile = new DataColumn("Result", typeof(string));
            DataColumn cItemGroup_DescFile = new DataColumn("ItemGroup_Desc", typeof(string));
            DataColumn cRepnoFile = new DataColumn("Repno", typeof(string));
            DataColumn cAmountFile = new DataColumn("Amount", typeof(int));
            DataColumn cLimit3File = new DataColumn("Limit3", typeof(int));
            DataColumn cLimit4File = new DataColumn("Limit4", typeof(int));
            DataColumn cLimit5File = new DataColumn("Limit5", typeof(int));
            DataColumn cLimit6File = new DataColumn("Limit6", typeof(int));
            DataColumn cLimit7File = new DataColumn("Limit7", typeof(int));

            myDataTableFile.Columns.AddRange(new DataColumn[]
                    { cShiftIDFile, cShiftStringFile, cPPPLGRFile, cRec_DateFile, cRec_TimeFile, cYearFile, cMonthFile, cDayFile, cHourFile, 
                        cMinuteFile, cSecondFile, cValueFile, cLimit1File, cLimit2File, cResultFile, cItemGroup_DescFile, cRepnoFile, cAmountFile,
                        cLimit3File, cLimit4File, cLimit5File, cLimit6File, cLimit7File });
            #endregion

            myDataSet.Tables.Add(myDataTable);
        }
    }
}
