﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" indent="yes"/>
  
  <xsl:template match="/">
        <table style="border: 2px">
          <xsl:apply-templates select="//assembly"/>
        </table>
  </xsl:template>

  <xsl:template match="assembly">
      <tr>
        <td>Projekt neve: <xsl:value-of select="name"/></td>
      </tr>
    <tr>
      <td>
        Osztály neve: <xsl:value-of select="$MemberName"/>
      </td>
    </tr>
  </xsl:template>




</xsl:stylesheet>
