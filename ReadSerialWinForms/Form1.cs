﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using FirebirdSql.Data.FirebirdClient;
using System.Collections;
using System.Net;

namespace ReadSerialWinForms
{
    /// <summary>
    /// Main form
    /// </summary>
    public partial class Form1 : Form
    {
        private PortReader myPortReader = new PortReader();
        Timer valueClock, logClock;
        int lastValue;
        bool logStatus;
        static List<string> spieces = new List<string>();
        /// <summary>
        /// Printers ON / OFF (SW / Home or Work)
        /// </summary>
        public static bool PrinterStatus { get; set; }
        public static readonly Where Where = Where.Home;
        
        /// <summary>
        /// Check connections, timers start
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            if (myPortReader.dbConnIsOpen == false)
            {
                MessageBox.Show("Nem elérhető az adatbázis a hálózaton keresztül.\n" +
                    "A program bezárul!", "Hiba!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Environment.Exit(0);
            }

            if (myPortReader.port2IsOpen == false)
            {
                MessageBox.Show("Nem elérhető a port.\n" +
                    "A program bezárul!", "Hiba!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Environment.Exit(0);
            }

            valueClock = new Timer();
            valueClock.Interval = 1000;
            valueClock.Start();
            valueClock.Tick += new EventHandler(ValueWrite);

            logClock = new Timer();
            logClock.Interval = 600000; // Clear listbox pro 10 minutes
            logClock.Start();
            logClock.Tick += new EventHandler(LogClear);

            myPortReader.StatusChangedEvent += new StatusDelegate(ChangedStatus);
            myPortReader.InputChangedEvent += new InputDelegate(ChangedInput);
            
            label1.Text = "0";  label2.Text = "0";  label3.Text = "0";
            label4.Text = "0";  label5.Text = "0";  label6.Text = "0";
            label7.Text = "0";

            label11.Text = "A naplózó ablak 10 percenként törlődik.";

            label20.Text = "";
            label21.Text = "";

            lastValue = 0;

            PrinterStatus = chBPrint.Checked;
            logStatus = chbLog.Checked;
        }

        /// <summary>
        /// It writes information on the form
        /// </summary>
        public void ValueWrite(object sender, EventArgs eArgs)
        {
            if (sender == valueClock)
            {
                // Every label 0
                if (lastValue != myPortReader.Value && label1.Text.Equals(0))
                {
                    label1.Text = myPortReader.Value.ToString();
                    lastValue = myPortReader.Value;
                    if (myPortReader.Result.Equals("DARK"))
                        label21.Text = "SOTET";
                    else if (myPortReader.Result.Equals("LIGHT"))
                        label21.Text = "VILAGOS";
                    else
                        label21.Text = "OK";
                }
                // Már vannak kiírt értékek és minden régebbi értéket csúsztat "hátrafelé".
                else if (lastValue != myPortReader.Value)
                {
                    label7.Text = label6.Text; label6.Text = label5.Text;
                    label5.Text = label4.Text; label4.Text = label3.Text;
                    label3.Text = label2.Text; label2.Text = label1.Text;
                    label1.Text = myPortReader.Value.ToString();
                    lastValue = myPortReader.Value;
                    //label21.Text = myPortReader.Result;
                    if (myPortReader.Result.Equals("DARK"))
                        label21.Text = "SOTET";
                    else if (myPortReader.Result.Equals("LIGHT"))
                        label21.Text = "VILAGOS";
                    else
                        label21.Text = "OK";
                }
                // The end of the batch are all value 0
                else if (lastValue == myPortReader.Value && lastValue == 0)
                {
                    label7.Text = label6.Text; label6.Text = label5.Text;
                    label5.Text = label4.Text; label4.Text = label3.Text;
                    label3.Text = label2.Text; label2.Text = label1.Text;
                    label1.Text = myPortReader.Value.ToString();
                    label21.Text = "";
                    //listBox2.Items.Clear();
                }

                // At 10 pm. Database log is cleared
                if(DateTime.Now.Hour == 22 && DateTime.Now.Minute == 0 && DateTime.Now.Second == 0)
                    listBox2.Items.Clear();
            }
        }

        /// <summary>
        /// Database log
        /// </summary>
        public void ChangedStatus()
        {
            if (listBox2.InvokeRequired)
            {
                listBox2.Invoke(new MethodInvoker(ChangedStatus));
                return;
            }
            
            listBox2.Items.Add(myPortReader.Status);
            listBox2.SelectedIndex = listBox2.Items.Count - 1;
        }

        /// <summary>
        /// Camera log
        /// </summary>
        public void ChangedInput()
        {
            if (listBox1.InvokeRequired)
            {
                listBox1.Invoke(new MethodInvoker(ChangedInput));
                return;
            }

            listBox1.Items.Add(myPortReader.Input);
            listBox1.SelectedIndex = listBox1.Items.Count - 1;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ChangedItemgroup()
        {
            if (label20.InvokeRequired)
            {
                label20.Invoke(new MethodInvoker(ChangedItemgroup));
                return;
            }
            
            label20.Text = myPortReader.ItemGroupDesc;
        }

        /// <summary>
        /// Camera log is deleted after 10 minutes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eArgs"></param>
        public void LogClear(object sender, EventArgs eArgs)
        {
            listBox1.Items.Clear();
        }

        /// <summary>
        /// A program bezárát végzi ez a függvény.
        /// </summary>
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                myPortReader.port.Close();
                myPortReader.port2.Close();
                if(myPortReader.Conn != null)
                    myPortReader.Conn.Close();
                
                myPortReader.com4Port.RtsEnable = false;
                myPortReader.com4Port.Close();
                MessageBox.Show("A port és az adatkapcsolat bezárt.", "Kilépés", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void modositasokToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ChangeLimits().ShowDialog();
        }

        private void jelszoModositasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ChangePassword().ShowDialog();
        }

        private void chbLog_CheckedChanged(object sender, EventArgs e)
        {
            logStatus = chbLog.Checked;
            if (logStatus == true)
            {
                listBox1.Visible = true;
                label11.Visible = true;
            }
            else
            {
                listBox1.Visible = false;
                label11.Visible = false;
            }
        }

        private void txb_repno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != 'ö'))
            {
                e.Handled = true;
            }
            if (e.KeyChar == 'ö')
            {
                e.KeyChar = '0'; // Barcode: ö = 0 (Hungarian keyboard)
            }
        }

        private void btn_inditas_Click(object sender, EventArgs e)
        {
            RepnoFafajEgyeztetesEsErtekadas();
            txb_repno.Select();
            txb_repno.SelectAll();
        }

        private void RepnoFafajEgyeztetesEsErtekadas()
        {
            myPortReader.RepnoID = txb_repno.Text;
            string dbfafaj = "", chbfafaj = cmb_fafaj.SelectedItem.ToString().ToUpper();
            
            #region MachineID --> select latest REPNO
            //if (myPortReader.where.Equals("SW"))
            if(Where == ReadSerialWinForms.Where.SW)
            {
                FileStream fs = new FileStream(@"C:\Egyetem\DataFile.dat", FileMode.Open);
                try
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    Hashtable fileEntries = (Hashtable)formatter.Deserialize(fs);
                    myPortReader.IP_Address = fileEntries["IPaddress"].ToString();
                }
                catch 
                {
                    throw;
                }
                finally
                {
                    fs.Close();
                }
                
                //if (myPortReader.where.Equals("SW"))
                if (Where == ReadSerialWinForms.Where.SW)
                    myPortReader.Conn = new FbConnection("secret");
                else
                    myPortReader.Conn = new FbConnection(@"Server=localhost;User=SYSDBA;Password=masterkey;Database=D:\Munka\Swedwood\TESZTEGY.FDB");
                myPortReader.Conn.Open();

                string CommandText = string.Format("SELECT ITEMGROUPS.ITEMGROUP_DESC " +
                    "FROM MWOOPE JOIN MWOHED ON MWOOPE.VOMFNO = MWOHED.VHMFNO " +
                    "JOIN MITMAS ON MITMAS.MMITNO = MWOHED.VHPRNO " +
                    "JOIN ITEMGROUPS ON ITEMGROUPS.ITEMGROUP_ID = RIGHT(MITMAS.MMITGR,4) " +
                    "WHERE MWOOPE.VOWOSQ = \'{0}\'", myPortReader.RepnoID);

                FbCommand command = new FbCommand(CommandText, myPortReader.Conn);
                FbDataReader fafajReader = command.ExecuteReader();

                while (fafajReader.Read())
                    dbfafaj = fafajReader.GetString(0).ToUpper();

                myPortReader.Conn.Close();

                myPortReader.Status = "Volt kapcsolat. Fafaj: " + dbfafaj;
                this.ChangedStatus();
            }
            else
                dbfafaj = chbfafaj;
            #endregion
            
            if (dbfafaj.ToUpper().Equals(chbfafaj) || dbfafaj.Equals(""))
            {
                ChangedItemgroup();
                myPortReader.ItemGroupDesc = chbfafaj;
                label20.Text = myPortReader.ItemGroupDesc;

                #region Kamera ini fájljába írás, majd felmásolás a kamera fájlrendszerébe
                                
                // FTP connection init
                string user, password, address, fulladdress;
                FileStream fs = new FileStream(@"C:\Egyetem\DataFile.dat", FileMode.Open);

                BinaryFormatter formatter = new BinaryFormatter();
                myPortReader.fileEntries = (Hashtable)formatter.Deserialize(fs);

                fs.Close();

                //if (myPortReader.where.Equals("SW"))
                if (Where == ReadSerialWinForms.Where.SW)
                {
                    address = myPortReader.fileEntries["KameraIP"].ToString();
                    fulladdress = "ftp://" + address + "/mnt/0/Tint/";
                    user = "secret";
                    password = "secret";
                }
                //else if (myPortReader.where.Equals("Home"))
                if (Where == ReadSerialWinForms.Where.Home)
                {
                    fulladdress = "ftp://195.228.155.242:1921/";
                    user = "secret";
                    password = "secret";
                }
                else
                {
                    fulladdress = "ftp://193.224.61.169/";
                    user = "secret";
                    password = "secret";
                }

                myPortReader.Status = "csatlakozás ide: " + fulladdress;
                this.ChangedStatus();
                
                // BiiSeqNumber download from FTP
                string inputfilepath = @"C:\Egyetem\BiiSeqNum.ini";                
                string ftpfilepath = "BiiSeqNum.ini";

                string ftpfullpath = fulladdress + ftpfilepath;

                using (WebClient request = new WebClient())
                {
                    request.Credentials = new NetworkCredential(user, password);
                    
                    byte[] fileData = request.DownloadData(ftpfullpath);
                    
                    
                    using (FileStream file = File.Create(inputfilepath))
                    {
                        file.Write(fileData, 0, fileData.Length);
                        file.Close();
                    }                    
                }

                myPortReader.Status = "sikeres fajlletoltes";
                this.ChangedStatus();
                
                // Get number from first (0.) row
                List<string> lines2 = new List<string>(File.ReadAllLines(inputfilepath));
                string number = lines2[0].ToString();
                                
                string path = @"C:\Egyetem\toUpload.ini";
                string path2 = @"C:\Egyetem\" + number + ".ini";
                
                myPortReader.Status = "Legutóbbi ini: "+path2;
                this.ChangedStatus();
                myPortReader.Status = "Legutóbbi fafaj: " + dbfafaj;
                this.ChangedStatus();

                List<string> lines = new List<string>(File.ReadAllLines(path));
                int lineIndex = lines.FindIndex(line => line.StartsWith("material = "));
                if (lineIndex != -1)
                {
                    lines[lineIndex] = "material = " + dbfafaj;
                    string[] s = lines.ToArray();
                    File.WriteAllLines(path, s);
                }

                // Rename file
                File.Copy(path, path2);
                
                //if (myPortReader.where.Equals("SW"))
                if (Where == ReadSerialWinForms.Where.SW)
                {
                    myPortReader.DoCopyOnFTP(number + ".ini");
                }

                File.Delete(path2);

                // After writing I will get and display it
                //var data = File
                //    .ReadAllLines(path)
                //    .Select(x => x.Split('='))
                //    .Where(x => x.Length > 1)
                //    .ToDictionary(x => x[0].Trim(), x => x[1]);
                //MessageBox.Show(data["material"].ToString());
                
                #endregion
            }
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            this.ActiveControl = txb_repno;
            cmb_fafaj.Items.Clear();
            // Get species
            FileStream fs = new FileStream(@"C:\Egyetem\DataFile2.dat", FileMode.Open);
            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                spieces = (List<string>)formatter.Deserialize(fs);
            }
            catch
            {
                throw;
            }
            finally
            {
                fs.Close();
            }
            cmb_fafaj.Items.AddRange(spieces.ToArray());
        }
    }
}
