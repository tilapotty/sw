﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Text.RegularExpressions;
using FirebirdSql.Data.FirebirdClient;
using System.Data;
using System.Threading;
using System.Net;
using System.Net.NetworkInformation;
using System.ComponentModel;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace ReadSerialWinForms
{
    partial class PortReader
    {
        private void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //if (where.Equals("SW"))
            if (Form1.Where == ReadSerialWinForms.Where.SW)
                input = port.ReadLine(); // SW: there is end of line character
            else
                input = port.ReadExisting(); // Home/Work: there isn't

            InputChangedEvent();

            #region Old: Regex/Match: itemGroupDesc --> Now: manual
            // Minta: Jan  1 08:03:07 Bi-i compactserver[216]: [Trace]-Read data from 'material' key: NYIRELO
            //Regex pattern = new Regex(@"(?<prefix>\S+\s+\d+\s(\d|\:)+\sBi-i\scompactserver\[\d+\]\:\s\[Trace\]-Read\sdata\sfrom\s\'material\'\skey\:\s)" +
            //    @"(?<fafaj>\S+)");
            #endregion
            
            // Pattern: Jan  3 18:33:02 Bi-i vftpd[264]: receive 00000444.ini in binary mode
            Regex pattern = new Regex(@"(?<prefix>\S+\s+\d+\s(\d|\:)+\sBi-i\svftpd\[\d+\]\:\sreceive\s\d+.ini\sin\sbinary\smode)");
            Match theMatch = pattern.Match(input);
            
            if (theMatch.Groups["prefix"].Length != 0)
            {
                if (flag == 1)
                {
                    // Ping is not working in SW
                    //this.Ping();
                    this.WriteToDatabase();
                }

                this.PullLimitsMID(itemGroupDesc);

                this.BatchDataCollecting(); // it contains: conn.Open()

                this.PushLastItemGroupDesc(itemGroupDesc);

                flag = 1;
            }

            #region Old: Regex/Match: limits from camera
            //Regex pattern00 = new Regex(@"(?<prefix>\S+\s+\d+\s(\d|\:)+\sBi-i\scompactserver\[\d+\]\:\s\[Trace\]-Read\sdata\sfrom\s\'\/mnt\/0\/tint\/TintApp.ini\'\sfile\sat\s\'WPMat_"
            //    + itemGroupDesc +
            //    @"\\ThreshDark\'\skey\:\s)" +
            //    @"(?<limit1>\d+)");

            //Match theMatch00 = pattern00.Match(input);
            //if (theMatch00.Groups["limit1"].Length != 0)
            //{
            //    limit1 = int.Parse(theMatch00.Groups["limit1"].ToString());
            //}

            //Regex pattern01 = new Regex(@"(?<prefix>\S+\s+\d+\s(\d|\:)+\sBi-i\scompactserver\[\d+\]\:\s\[Trace\]-Read\sdata\sfrom\s\'\/mnt\/0\/tint\/TintApp.ini\'\sfile\sat\s\'WPMat_"
            //    + itemGroupDesc +
            //    @"\\ThreshLight\'\skey\:\s)" +
            //    @"(?<limit2>\d+)");

            //Match theMatch01 = pattern01.Match(input);
            //if (theMatch01.Groups["limit2"].Length != 0)
            //{
            //    limit2 = int.Parse(theMatch01.Groups["limit2"].ToString());
            //}
            #endregion

            #region Limits from INI file and the last itemGroupDesc save
            //if (!itemGroupDesc.Equals(""))
            //{
            //    limit1 = int.Parse(parser.GetSetting(itemGroupDesc, "1GroupLimit"));
            //    limit2 = int.Parse(parser.GetSetting(itemGroupDesc, "2GroupLimit"));
            //    limit3 = int.Parse(parser.GetSetting(itemGroupDesc, "3GroupLimit"));
            //    limit4 = int.Parse(parser.GetSetting(itemGroupDesc, "4GroupLimit"));
            //    limit5 = int.Parse(parser.GetSetting(itemGroupDesc, "5GroupLimit"));
            //    limit6 = int.Parse(parser.GetSetting(itemGroupDesc, "6GroupLimit"));
            //    limit7 = int.Parse(parser.GetSetting(itemGroupDesc, "7GroupLimit"));
            //    //A legutóbbi fafaj mentése
            //    parser.AddSetting("ITEMGROUPDESC", "LAST", itemGroupDesc);
            //    parser.SaveSettings();
            //}
            //else
            //{
            //    itemGroupDesc = parser.GetSetting("ITEMGROUPDESC", "LAST");
            //}
            #endregion

            #region Regex/Match: Value, Result, Darab
            Regex pattern1 = new Regex(@"(?<prefix>\S+\s+\d+\s(\d|\:)+\sBi-i\scompactserver\[\d+\]\:\s\[Warning\]-Result\:\s\S+\s\(avg\:)" +
                @"(?<value>\d+)" +
                @"(?<postfix>\))");

            Match theMatch1 = pattern1.Match(input);
            
            if (theMatch1.Groups["value"].Length != 0 && flag == 1)
            {
                value = int.Parse(theMatch1.Groups["value"].ToString());
                
                // Result from value and limits
                if      (value < limit1)                   { result = "DARK"; }
                else if (value >= limit1 && value < limit2) { result = "OK"; }
                else if (value >= limit2 && value < limit3) { result = "LIGHT"; }
                else if (value >= limit3 && value < limit4) { result = "4Group"; }
                else if (value >= limit4 && value < limit5) { result = "5Group"; }
                else if (value >= limit5 && value < limit6) { result = "6Group"; }
                else if (value >= limit6 && value < limit7) { result = "7Group"; }
                else if (value >= limit7)                    { result = "8Group"; }
                
                // Increasing amount of an existing value
                if (valueAndAmount.ContainsKey(value))
                {
                    valueAndAmount[value]++;
                    amount = valueAndAmount[value];

                    DataRow[] valueRow = myDataSet.Tables["ELOGYALULASLOG"].Select("Value = '" + value + "'");
                    valueRow[0]["Amount"] = amount;
                }
                else
                {
                    // New value is coming
                    valueAndAmount.Add(value, 1);
                    amount = valueAndAmount[value]; // amount = 1

                    DataRow actDataRow = myDataTable.NewRow();

                    actDataRow["ShiftID"] = shiftID;    actDataRow["ShiftString"] = shiftString;
                    actDataRow["PPPLGR"] = machineID;

                    actDataRow["Rec_Date"] = recDate.ToString("yyyy.MM.dd.");   actDataRow["Rec_Time"] = recTime;
                    actDataRow["Year"] = year;  actDataRow["Month"] = month;    actDataRow["Day"] = day;
                    actDataRow["Hour"] = hour;  actDataRow["Minute"] = minute;  actDataRow["Second"] = second;

                    actDataRow["Value"] = value;
                    actDataRow["Limit1"] = limit1; actDataRow["Limit2"] = limit2;
                    actDataRow["Limit3"] = limit3; actDataRow["Limit4"] = limit4;
                    actDataRow["Limit5"] = limit5; actDataRow["Limit6"] = limit6;
                    actDataRow["Limit7"] = limit7;
                    
                    actDataRow["Result"] = result;

                    actDataRow["ItemGroup_Desc"] = itemGroupDesc;
                    actDataRow["Repno"] = repnoID;
                    actDataRow["Amount"] = amount;

                    myDataTable.Rows.Add(actDataRow);
                }
                
                #region DataRow is writed into CSV file - Case: amount = 1, because of "live saving"
                DataRow actDataRowFile = myDataTableFile.NewRow();

                actDataRowFile["ShiftID"] = shiftID; actDataRowFile["ShiftString"] = shiftString;
                actDataRowFile["PPPLGR"] = machineID;

                actDataRowFile["Rec_Date"] = recDate; actDataRowFile["Rec_Time"] = recTime;
                actDataRowFile["Year"] = year; actDataRowFile["Month"] = month; actDataRowFile["Day"] = day;
                actDataRowFile["Hour"] = hour; actDataRowFile["Minute"] = minute; actDataRowFile["Second"] = second;

                actDataRowFile["Value"] = value;
                actDataRowFile["Limit1"] = limit1; actDataRowFile["Limit2"] = limit2;
                actDataRowFile["Limit3"] = limit3; actDataRowFile["Limit4"] = limit4;
                actDataRowFile["Limit5"] = limit5; actDataRowFile["Limit6"] = limit6;
                actDataRowFile["Limit7"] = limit7;

                actDataRowFile["Result"] = result;

                actDataRowFile["ItemGroup_Desc"] = itemGroupDesc;
                actDataRowFile["Repno"] = repnoID;
                actDataRowFile["Amount"] = 1;

                myDataTableFile.Rows.Add(actDataRowFile);
                string filename = @"C:\Egyetem\adatok\" + DateTime.Now.ToString("yyyyMMdd") +".csv";
                this.DataRowSaveToFile(actDataRowFile, filename, ";");
                myDataTableFile.Clear();
                #endregion
            }
            
            #endregion
        }

        private void Barrier_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //if (where.Equals("SW"))
            if (Form1.Where == ReadSerialWinForms.Where.SW)
            {
                barrierInput = port2.ReadByte().ToString(); // SW: 0 value is coming
            }
            else
            {
                barrierInput = port2.ReadExisting(); // Home/Work: there is no end of line character
            }

            if (!barrierInput.Equals(""))
            {
                barrierFlag = true;
                barrierInput = "";
            }
            
            // Print the result with the correct printer
            if (Form1.PrinterStatus == true && barrierFlag == true && !result.Equals(""))
            {
                switch (result)
                {
                    case "DARK":
                        printerValue = (int)Math.Pow(2, 0);
                        break;
                    case "OK":
                        printerValue = (int)Math.Pow(2, 1); // Hidromat: no printing, Selector machine: printing
                        break;
                    case "LIGHT":
                        printerValue = (int)Math.Pow(2, 2);
                        break;
                    case "4Group":
                        printerValue = (int)Math.Pow(2, 3);
                        break;
                    case "5Group":
                        printerValue = (int)Math.Pow(2, 4);
                        break;
                    case "6Group":
                        printerValue = (int)Math.Pow(2, 5);
                        break;
                    case "7Group":
                        printerValue = (int)Math.Pow(2, 6);
                        break;
                    case "8Group":
                        printerValue = (int)Math.Pow(2, 7);
                        break;
                }

                //if (where.Equals("SW"))
                if (Form1.Where == ReadSerialWinForms.Where.SW)
                {
                    Lpt.Output(888, printerValue);
                    Thread.Sleep(300); // time of voltage on the LPT port
                    Lpt.Output(888, 0);
                }
                else
                {
                    MessageBox.Show(value.ToString() + "\n" + result + "\n" + printerValue);
                }
                    
                barrierFlag = false;
                printerValue = 0;
                //timeDelay = 300;
            }
        }
        #region Push/Pull properties from the DataFile.dat file
        [STAThread]
        private void PushLastItemGroupDesc(string specie)
        {
            fileEntries["lastItemGroupDesc"] = specie;
            
            FileStream fs = new FileStream(@"C:\Egyetem\DataFile.dat", FileMode.Create);

            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, fileEntries);
            }
            catch
            {
                throw;
            }
            finally
            {
                fs.Close();
            }
        }
        
        [STAThread]
        private void PullLimitsMID(string specie)
        {
            FileStream fs = new FileStream(@"C:\Egyetem\DataFile.dat", FileMode.Open);
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                fileEntries = (Hashtable)formatter.Deserialize(fs);
            }
            catch
            {
                throw;
            }
            finally
            {
                fs.Close();
            }
            specie = specie.ToLower();
            string tempHashKey = specie + "1G";
            limit1 = int.Parse(fileEntries["" + tempHashKey + ""].ToString());
            tempHashKey = specie + "2G";
            limit2 = int.Parse(fileEntries["" + tempHashKey + ""].ToString());
            tempHashKey = specie + "3G";
            limit3 = int.Parse(fileEntries["" + tempHashKey + ""].ToString());
            tempHashKey = specie + "4G";
            limit4 = int.Parse(fileEntries["" + tempHashKey + ""].ToString());
            tempHashKey = specie + "5G";
            limit5 = int.Parse(fileEntries["" + tempHashKey + ""].ToString());
            tempHashKey = specie + "6G";
            limit6 = int.Parse(fileEntries["" + tempHashKey + ""].ToString());
            tempHashKey = specie + "7G";
            limit7 = int.Parse(fileEntries["" + tempHashKey + ""].ToString());

            machineID = fileEntries["PPPLGR"].ToString();
            IPaddress = fileEntries["IPaddress"].ToString();
            timeDelay = int.Parse(fileEntries["TimeDelay"].ToString());           
        }
        #endregion

        /// <summary>
        /// It collects the information for the following batch
        /// date, time, shift ID, shift description, batch ID (REPNO)
        /// </summary>
        private void BatchDataCollecting()
        {
            // Set date and time
            recDate = DateTime.Now;
            year = recDate.Year;
            month = recDate.Month;
            day = recDate.Day;
            hour = recDate.Hour;
            minute = recDate.Minute;
            second = recDate.Second;
            recTime = new TimeSpan(hour, minute, second);

            // Set shift ID
            // Set shiftstring: DE -> DU -> EJ
            if (hour >= 6 && hour < 14)
            {
                shiftID = 1;
                shiftString = recDate.ToString("yyMMdd") + "DE"; // pl.: 110501DE (2011.05.01. DE)
            }
            else if (hour >= 14 && hour < 22)
            {
                shiftID = 2;
                shiftString = recDate.ToString("yyMMdd") + "DU"; // DU
            }
            else if (hour >= 22 && hour < 24)
            {
                shiftID = 0;
                shiftString = recDate.ToString("yyMMdd") + "EJ"; // EJ
            }
            else if (hour >= 0 && hour < 6)
            {
                shiftID = 0;
                shiftString = recDate.AddDays(-1).ToString("yyMMdd") + "EJ"; // EJ
            }

            //if (where.Equals("SW"))
            if (Form1.Where == ReadSerialWinForms.Where.SW)
            {
                conn = new FbConnection("secret");
            }
            else
            {
                conn = new FbConnection(@"Server=localhost;User=SYSDBA;Password=masterkey;Database=D:\Munka\Swedwood\TESZTEGY.FDB");
            }

            #region Old: last REPNO from database to the selected machine --> Now: manual
            //conn.Open();

            //string CommandText = string.Format("SELECT REPNO FROM LOG_LAST_INSERT WHERE PPPLGR = \'{0}\'", machineID);
            //FbCommand command = new FbCommand(CommandText, conn);
            //FbDataReader repnoReader = command.ExecuteReader();

            //while (repnoReader.Read())
            //    repnoID = repnoReader.GetString(0);
            
            //conn.Close();
            #endregion
        }
        
        #region It is not working in SW. Ping, SaveToFile, if the machine is online again, then SaveToDatabase
        private void Ping()
        {
            string who;

            //if (where.Equals("SW"))
            if (Form1.Where == ReadSerialWinForms.Where.SW)
                who = IPaddress;   // SW: DB Server IP
            //else if (where.Equals("Work"))
            else if (Form1.Where == ReadSerialWinForms.Where.Work)
                who = "10.16.4.254";
            else
                who = "192.168.1.1";    // Home

            AutoResetEvent waiter = new AutoResetEvent(false);

            Ping pingSender = new Ping();

            // When the PingCompleted event is raised,
            // the PingCompletedCallback method is called.
            pingSender.PingCompleted += new PingCompletedEventHandler(PingCompletedCallback);

            // Create a buffer of 32 bytes of data to be transmitted.
            string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes(data);

            // Wait 0,5 second for a reply.
            int timeout = 500;

            // Set options for transmission:
            // The data can go through 64 gateways or routers before it is destroyed, and the data packet
            // cannot be fragmented.
            PingOptions options = new PingOptions(64, true);

            // Send the ping asynchronously.
            // Use the waiter as the user token.
            // When the callback completes, it can wake up this thread.
            pingSender.SendAsync(who, timeout, buffer, options, waiter);

            // Prevent this example application from ending. A real application should do something useful
            // when possible.
            waiter.WaitOne();
        }
        
        private void PingCompletedCallback(object sender, PingCompletedEventArgs e)
        {
            // If the operation was canceled, display a message to the user.
            if (e.Cancelled)
            {
                // Let the main thread resume. 
                // UserToken is the AutoResetEvent object that the main thread is waiting for.
                ((AutoResetEvent)e.UserState).Set();
            }

            // If an error occurred, display the exception to the user.
            if (e.Error != null)
            {
                // Let the main thread resume. 
                ((AutoResetEvent)e.UserState).Set();
            }

            PingReply reply = e.Reply;

            DisplayReply(reply);

            // Let the main thread resume.
            ((AutoResetEvent)e.UserState).Set();
        }
        
        /// <summary>
        /// Check: Is the machine online?
        /// Available: write into database
        /// Not: write into file
        ///     Later available: file and current datatable are writed into database
        /// </summary>
        private void DisplayReply(PingReply reply)
        {
            string filename = @"C:\Egyetem\Temp.csv";

            if (reply.Status == IPStatus.Success)
            {
                // If the CSV file exists, then ...
                // 1. Data of CSV are inserted into the DataTable
                if (File.Exists(filename))
                {
                    string[] values = File.ReadAllLines(filename);
                    string[] fields;

                    DataRow row;
                    for (int i = 0; i < values.GetLength(0); i++)
                    {
                        fields = values[i].Split(new char[] { ';' });
                        row = myDataTable.NewRow();
                        for (int f = 0; f < myDataTable.Columns.Count; f++)
                        {
                            row[f] = fields[f];
                        }
                        myDataTable.Rows.Add(row);
                    }
                    // 2. Delete the CSV file
                    File.Delete(filename);
                }
                
                this.WriteToDatabase();
            }
            else
            {
                this.DataTableSaveToFile(myDataTable, filename, ";");
            }

            myDataTable.Clear();
        }
        
        private void DataTableSaveToFile(DataTable table, string filename, string seperateChar)
        {
            StreamWriter sr = null;
            try
            {
                sr = new StreamWriter(filename, true);
                StringBuilder builder = new StringBuilder();
                foreach (DataRow row in table.Rows)
                {
                    foreach (DataColumn col in table.Columns)
                    {
                        builder.Append(row[col.ColumnName]).Append(seperateChar);
                    }
                    sr.WriteLine(builder.ToString());
                }
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                }
            }
        }
        
        private void DataRowSaveToFile(DataRow row, string filename, string seperateChar)
        {
            StreamWriter sr = null;
            try
            {
                sr = new StreamWriter(filename, true);
                StringBuilder builder = new StringBuilder();
                foreach (var cell in row.ItemArray)
                {
                    builder.Append(cell).Append(seperateChar);
                }
                sr.WriteLine(builder.ToString());
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                }
            }
        }

        #endregion

        /// <summary>
        /// Online: write into database
        /// Keep Status up-to-date.
        /// </summary>
        private void WriteToDatabase()
        {
            //if (where.Equals("SW"))
            if (Form1.Where == ReadSerialWinForms.Where.SW)
            {
                conn = new FbConnection("secret");
            }
            else
            {
                conn = new FbConnection(@"Server=localhost;User=SYSDBA;Password=masterkey;Database=D:\Munka\Swedwood\TESZTEGY.FDB");
            }
            
            conn.Open();

            status = "Adatkapcsolat létrehozva.";

            StatusChangedEvent();

            string tempCommandText;
            int osszeg = 0;
            for (int curRow = 0; curRow < myDataTable.Rows.Count; curRow++)
            {
                DateTime tempDate = (DateTime)myDataTable.Rows[curRow][3];

                //SW: 12 hidromat, SW elosztós 10, egyébként 10
                string tempDateString;
                if (machineID.Equals("1170-1"))
                    tempDateString = tempDate.ToShortDateString().Remove(12);
                else
                    tempDateString = tempDate.ToShortDateString().Remove(10);
                
                // ID is generated automatically
                tempCommandText = string.Format(
                    "Insert into ELOGYALULASLOG(ShiftID, ShiftString, PPPLGR, " +
                    "Rec_Date, Rec_Time, FYear, FMonth, FDay, FHour, FMinute, FSecond, " +
                    "FValue, Limit1, Limit2, Result, ItemGroup_Desc, Repno, Darab, " +
                    "Limit3, Limit4, Limit5, Limit6, Limit7) " +
                    "values({0}, \'{1}\', \'{2}\', \'{3}\', \'{4}\', {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, \'{14}\', \'{15}\', \'{16}\', {17}, {18}, {19}, {20}, {21}, {22});",
                    myDataTable.Rows[curRow][0], myDataTable.Rows[curRow][1], myDataTable.Rows[curRow][2],
                    tempDateString, myDataTable.Rows[curRow][4], myDataTable.Rows[curRow][5],
                    myDataTable.Rows[curRow][6], myDataTable.Rows[curRow][7], myDataTable.Rows[curRow][8],
                    myDataTable.Rows[curRow][9], myDataTable.Rows[curRow][10], myDataTable.Rows[curRow][11],
                    myDataTable.Rows[curRow][12], myDataTable.Rows[curRow][13], myDataTable.Rows[curRow][14],
                    myDataTable.Rows[curRow][15], myDataTable.Rows[curRow][16], myDataTable.Rows[curRow][17],
                    myDataTable.Rows[curRow][18], myDataTable.Rows[curRow][19], myDataTable.Rows[curRow][20],
                    myDataTable.Rows[curRow][21], myDataTable.Rows[curRow][22]);
                
                FbCommand command = new FbCommand(tempCommandText, conn);

                command.ExecuteNonQuery();
                osszeg += int.Parse(myDataTable.Rows[curRow][17].ToString());
            }

            status = "Adatfeltöltés: " + osszeg + " darab";
            StatusChangedEvent();

            value = 0;
            valueAndAmount.Clear();
            myDataTable.Clear();

            conn.Close();
            status = "Adatkapcsolatzárás.";
            StatusChangedEvent();
        }
        
        #region Save password into the Password.dat file
        public static string GetMd5Hash(MD5 md5Hash, string input)
        {
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        // Verify a hash against a string.
        public static bool VerifyMd5Hash(MD5 md5Hash, string input)
        {
            string hash;

            // Open the file containing the data that you want to deserialize.
            FileStream fs = new FileStream(@"C:\Egyetem\Password.dat", FileMode.Open);
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();

                // Deserialize the hashtable from the file and 
                // assign the reference to the local variable.
                addresses = (Hashtable)formatter.Deserialize(fs);
            }
            catch
            {
                throw;
            }
            finally
            {
                fs.Close();
            }
            
            hash = addresses["Passwd"].ToString();
            
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            
            if (comparer.Compare(hashOfInput, hash) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        public void DoCopyOnFTP(string fileName)
        {
            try
            {
                //FTP initializing
                string user, password, address, fulladdress;
                FileStream fs = new FileStream(@"C:\Egyetem\DataFile.dat", FileMode.Open);
                                
                BinaryFormatter formatter = new BinaryFormatter();
                fileEntries = (Hashtable)formatter.Deserialize(fs);
                
                fs.Close();

                //if (where.Equals("SW"))
                if (Form1.Where == ReadSerialWinForms.Where.SW)
                {
                    address = fileEntries["KameraIP"].ToString();
                    fulladdress = "ftp://" + address + "/mnt/0/Tint/" + fileName;
                    user = "secret";
                    password = "secret"; // via .pdf (camera documentation)
                }
                else
                {
                    fulladdress = "ftp://193.224.61.169/" + fileName;
                    user = "secret";
                    password = "secret";
                }

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(fulladdress);

                //request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Method = WebRequestMethods.Ftp.UploadFile;

                // Login parameters
                request.Credentials = new NetworkCredential(user, password);

                Stream requestStream = request.GetRequestStream();

                // Copy file             
                StreamReader sourceStream = new StreamReader(@"C:\Egyetem\" + fileName);
                byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                sourceStream.Close();
                request.ContentLength = fileContents.Length;

                requestStream.Write(fileContents, 0, fileContents.Length);
                requestStream.Close();

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                response.Close();

                #region Test: for reading (see dictionary structure on FTP)
                //FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                //Stream responseStream = response.GetResponseStream();
                //StreamReader reader = new StreamReader(responseStream);

                //MessageBox.Show(reader.ReadToEnd());
                //MessageBox.Show("Directory List Complete, status {0}", response.StatusDescription);

                //reader.Close();
                //response.Close();
                #endregion
            }

            catch (WebException webex)
            {
                MessageBox.Show("Hiba kapcsolódáskor: " + webex.Message);              
            }
        }
    }
}
