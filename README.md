# Introduction #
We have chosen a furniture production company to illustrate the IT challenges of a smart production system. Before the implementation of the system, the company’s manufacturing processes were ana-lyzed. Our first objective was to reduce the ratio of waste products. Stocks are received from several suppliers during the provisioning. These stocks include wood materials. At the reception, a sampling procedure is done to assess the quality of the shipment (whether sampled material is straight for example). If a certain percent is deemed adequate, the shipment is accepted. In the next process, the received shipment is sorted by size.

In the chosen company, the first production step is the alignment of the timber board. Then a camera determines wooden material color which is a really important property for the production line. The wooden material color has a lower and upper limits and the color is sufficient, when the measured color value is between the limits. However, if a measured value is under the first limit, then the wooden material having dark color must be handled another ways in later production phases as timber boards with the proper value to achieve the proper quality. Similarly, a wooden material with light color value (upper measured value as second limit), can also be signed for the later work. Since the production company use several wood species and each specie has its own limits and the color can depend on plane machine performance, the problem is getting more complex. But defining the color remains critical and the wrong decisions led to high refuse material rate.

To reduce refuse rate, a monitoring and controlling system was developed. The supported process is the following. First, an operator pulls down a barcode. The barcode identifies two kind of information: (1) The wooden material shipment and (2) one of the wood species (e.g., beech, birch, oak). 

Due to the wooden material shipment information, the measurement can be connected to the given vendor. In the second process phase, the wooden material is planed by a plane machine, then the timber color is determined by a camera in real time. The most waste products are created at this step. The automatic qualification is somewhat outdated, manual qualification can never be perfect as people make errors. Near the vendor identifier, one of the wood species, and the timber board color, the following metadata is at-tached: shift identifier, date, time and color result etc.

The third step is data uploading into the company database, while the current measurement values is presented on the client computer screen. The presentation is an information to the operator, who can react immediately if wrong values appear. The production support system is connected to three printers. The last step is to label the timber boards according to the color value, so dark, ok and light sign can be printed on timber boards. The process is repeated with a new wooden material. The whole measurement is sum-marized in Figure 1.
![Figure1_EnvironmentOfTheProductionProgress.PNG](https://bitbucket.org/repo/AzyMEE/images/3176226078-Figure1_EnvironmentOfTheProductionProgress.PNG)

The system provides an administration desktop as well where an administrator can change the production machine identifier and database IP address. Since the company has several plain machine and the internal informatics infrastructure can be changed, the production support system can be applied in these situa-tions without new IT development. The company can order several new wooden material, therefore new wood specie can also be added with its limits to the system. The production environments change always, so current limit values can also be changed with administrator privileges.

# The monitoring hardware components #
Image and video processing is a solution used in several industrial areas:
(1)	Security camera systems;
(2)	Medical image processing; 
(3)	Mapping of agricultural operations, etc. 
Industrial image and video processing systems are able to monitor quantity and quality levels of manu-factured products. Our system analyzes the color of timber board after the alignment. This task has to be done 24/7 in 3 shifts continuously. The system has to work without errors, otherwise the manufacturing process will stop causing extra cost. A few of the many advantages of our image and video processing system are the following: 
(1)	The burden of monotonous task on human operators can be reduced;
(2)	Accidental errors can be avoided;
(3)	Objective measurement instead of a subjective one;
(4)	Data collection can be automated. 
As a consequence, current and real-time statistics can be observed during the whole operation.

The multiple components of the system are camera, camera lenses, mounting frame, lighting, computer, data collection software. The factory’s environmental conditions can affect the measurement process, which has to be protected from these environmental factors. One of the most difficult tasks is to protect the system from dust. Any kind of analyzing algorithm will be employed, environmental factors will play a key role, which can ruin the algorithmically calculated results of the system. Lighting has to be kept at a fixed level and external light sources have to be closed out. Camera has to be also mounted in a fixed position, shaking cannot be tolerated to achieve consistent results. Lens selection is crucial, and the camera resolution is also important. These parameters are affected by the distance from the subject and the viewport, and by the industrial standard used to connect the camera and the lenses.
Lighting largely affects image processing, the main types are: 
(1)	Direction: from the bottom or from the top;
(2)	Light rays can be parallel or diffuse;
(3)	Angle of incidence can be big or small, because of this the viewport can light or dark. 

To achieve the best result, the parameters in previous settings can be combined. The light source has to operate according to CIE standards, the most widely used one is D65 (~6504K), which equals to daylight. As wood material is opaque, we use a light source on the top. Viewport will be bright, because many of the rays from the light will go to the camera. So the image will be darker at places, where the light direction is distorted by a surface error (scratches, contamination.) It is advisable to set rays from light source to be parallel. Diffuse light can enter the cracks on wood reducing the effectiveness of error detection.

# The monitoring and analyzing software solution #
In the view of information technology, the production support system must communicate with a lot of individual solutions. The vendor identifier and wood specie come from MOVEX ERP. The database management system is Firebird. The camera communicates with the client machine through serial port. Another serial port is used for the connection with the illuminating barrier, which starts the printer phase. The printing is performed with LPT  ports. The system operation is illustrated in Figure 2.
![Figure2_ScreenOfTheMonitoringApplication.png](https://bitbucket.org/repo/AzyMEE/images/2445255919-Figure2_ScreenOfTheMonitoringApplication.png)

Figure 2. Screen of the monitoring application (Note: The production support system was developed in Microsoft .NET framework and programmed in C# language.)

The system was operated over the last 4 years in the company and it was used to rank the vendor per-formance. The good the wooden material color, i.e., color value is between limits, the good the vendor is. Having ranking, the company reduced the refuse material rate by selecting good vendors and shipments are addressed to them. The research is continued in two directions: 
(1)	A camera system can be integrated into our self-developed solution;
(2)	Further and more complex analysis can be done.
The production progress is handled by the monitoring system which solves general IT problems. However, we would like to select a specific problem’s solution and introduce it. There were at least three industrial printers that we have to control at the same time, but we have only one LPT port in the controlling computer. For the solution, we were cooperating with electricians. The LPT port has eight data pin and the first three ones were linked with the printers. By sending a signal on the correct data pin, the correct printer started to print on the timber board. In this way, we are able control maximum eight different printers.